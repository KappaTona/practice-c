# Implement socket server-clien example
Found in `man getaddrinfo` under the EXAMPLE section.
Starting the server on given port, it waits for a client to send a message.
If a client succefully connected and sent a message then the server sends it back.
If the client sent the string "exit" the server will shutdown.


## Thank you
Huge thanks for the manual which opened my eyes that
sending a string through an open port is not an intimidating effort.
Mandatory gratitude towards Arch Linux which is my operation system for years
and I can not imagine my coding efforts without it.


## Description about my idea here
- The idea is, that I would like to practice C and learn more about socket based server client program using the UDP protocol, since it is the recommended way. For this to achieve I turn to the linux manual sections such as `man 2 socket`; `man 3 getaddrinfo`. They found locally on Arch linux after installing the [man-db](https://archlinux.org/packages/?name=man-db) and [man-pages](https://archlinux.org/packages/?name=man-pages).
- Also learn about [CMocka](https://cmocka.org/) which is the go-to technology for unittesting server-client based programs written in C.
- So this comment is years after the last commit. It seems that some Compilers are not allowing implicit function declration so the `modular approach: duplication of function prototypes with defines to avoid implicit func decl seems to be redundant`


##  Dependencies
- GCC and glibc for compiling the source and for socket, and netdb headers.
- CMocka if you want to build unittest


## Build Linux
- `cmake -S . -B build`
    - if you want to build Unittest add: `-DBUILD_TEST=ON`
- `cmake --build build`


## Usage
- server: ./server port
- client: ./client host port msg
    - if msg is the "exit" string then it is a msg to the server to be shutdown


## License
License is DO WHAT YOU WANT PUBLIC ![LiC](./LICENSE)


## Project status
- Observe server-client usage from `man getaddrinfo` example.
